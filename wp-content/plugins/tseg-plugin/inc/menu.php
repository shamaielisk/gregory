<?php

function setup_tseg_plugin_admin_menus() {

	$page_title = 'TSEG Options';
	$menu_title = 'TSEG';
	$capability = 'manage_options';
	$menu_slug = 'tsegoptions';
	$function = 'tseg_general_menu_function';

	add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function);
}
add_action('admin_menu', 'setup_tseg_plugin_admin_menus');

function tseg_general_menu_function() {

	?>
	<div class="wrap">
		<h2>TSEG Options</h2>
		<p class="description">There are currently no options. Check the sub-menus.</p>
	</div>
	<?php
}
