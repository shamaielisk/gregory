<?php

function setup_tseg_schema_menu() {

	$parent_slug = 'tsegoptions';
	$page_title = 'Schema';
	$menu_title = 'Schema';
	$capability = 'manage_options';
	$menu_slug = 'tsegschemaoptions';
	$function = 'tseg_schema_menu_function';

	add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
}
add_action('admin_menu', 'setup_tseg_schema_menu');

function register_tseg_schema_settings(){

	// create schema section
	add_settings_section(
		'schema_settings_section',
		'',
		'schema_options_callback',
		'tsegschemaoptions'
	);

	// create schema textarea
	add_settings_field(
		'schema',
		'schema',
		'schema_callback',
		'tsegschemaoptions',
		'schema_settings_section'
	);

	// check if options exist - add if do not currently exist
	if(get_option('schema') == null){
        add_option('schema');
    }

	// register our settings
	register_setting( 'tsegschemaoptions', 'schema');
}
add_action ('admin_init', 'register_tseg_schema_settings');

function schema_options_callback(){
    //
}

function schema_callback(){

    $schema = get_option('schema');
    $html = '<textarea rows="8" cols="50" name="schema" id="schema">' . $schema . '</textarea>';

    echo $html;
}

function tseg_schema_menu_function(){

	?>
	<div class="wrap">
    		<h1>TSEG Schema Editor</h1>
    		<p class="description">Don't forget to save!</p>
    		<form method="post" action="options.php">
        		<?php settings_fields( 'tsegschemaoptions' ); ?>
        		<?php do_settings_sections( 'tsegschemaoptions' ); ?>
        		<?php submit_button(); ?>
		</form>
	</div>
	<?php
}

function tseg_add_schema_to_footer(){

	// defining my variables
	$schema = get_option('schema');

	if($schema !=''){
		echo '<div style="display:none">' . $schema . '</div>';
	}

}
add_action( 'wp_footer', 'tseg_add_schema_to_footer');
