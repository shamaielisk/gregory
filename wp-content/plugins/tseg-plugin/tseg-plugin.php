<?php

/*
    Plugin Name: TSEG Plugin
    Author: Dan Laugharn, et al
    Description: A remotely-hosted, self-updating plugin that does stuff for our sites.
    Version: 0.7
*/


// Enable remote hosting updatability... gonna work on this language
require 'plugin-update-checker-4.1/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://s3-us-west-2.amazonaws.com/tseg-plugins/metadata.json',
    __FILE__,
    'tseg-plugin'
);


require 'inc/menu.php';
require 'inc/schema.php';


// Enable background automatic updates, and turn off the email notifications
add_filter( 'auto_update_core', '__return_true' );
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_core_update_send_email', '__return_false' );


// Enable Featured Images in pages
function featured_image_setup() {
    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'featured_image_setup');


// Enable the alive & well custom headers
function tseg_alive_and_well($headers){

    if(!is_admin()):
        $blog_public = get_option('blog_public');
        $headers['WP-Hack-Check'] = true;
        $headers['WP-Blog-Public'] = $blog_public;
    endif;

    return $headers;
}
add_filter('wp_headers', 'tseg_alive_and_well');
