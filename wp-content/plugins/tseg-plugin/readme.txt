== Changelog ==

= 0.7 =
* Plugin directory bug still present. Update Plugin Update Checker library from 2.0.0 to 4.1.

= 0.6 =
* Attempt to fix bug where plugin directory is renamed after update, causing WordPress to deactivate it.

= 0.5 =
* Added function to enable Featured Images in pages.

= 0.4 =
The last version that Dan released.

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.