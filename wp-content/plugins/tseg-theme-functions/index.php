<?php
/*
    Plugin Name: TSEG Theme Functions
    Description: Base functionality for TSEG themes. Creates [firm-name], [phone-number], and [children] shortcodes. Adds call tracking functionality. Shortcodes require an 'admin' user.
    Version: 2017.02.13
    Author: The Search Engine Guys
    Author URI: http://www.thesearchengineguys.com/
*/

include_once('inc/children.php'); // tseg_list_children function and shortcode. Used to list children pages with passable arguments.
include_once('inc/cleanup.php'); // Header Cleanup, Robots.txt Edits
include_once('inc/contact-methods.php'); // Additional user meta and the ability to call it
include_once('inc/title.php'); // tseg_title function. Generates the title from the meta with a basic page title fallback.

?>