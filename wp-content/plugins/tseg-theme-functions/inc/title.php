<?php
function tseg_title(){
    global $post;

    $meta_title = get_post_meta($post->ID,'title',true);
    $meta_description = get_post_meta($post->id,'description',true);
    $meta_keywords = get_post_meta($post->id,'keywords',true);

    echo $meta_title;
}
?>