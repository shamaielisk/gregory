<?php
function tseg_list_page_children(){
    global $post;
    $parent = $post->ID;
    $list_page_child_links = wp_list_pages('sort_column=post_date&sort_order=desc&child_of='.$parent.'&title_li=&depth=1&echo=0');

    return '<div class="tseg-children"><ul class="tseg-list">'.$list_page_child_links.'</ul></div>';
}

function tseg_children_shortcode($atts){
    global $post;

    extract(shortcode_atts(array(
        "parent"    =>  null,
        "depth"     =>  null,
        "exclude"   =>  null,
    ),$atts));

    if(!(empty($parent))){
        $child_links = wp_list_pages('child_of='.$parent.'&exclude='.$exclude.'&title_li=&echo=0');
        return '<ul class="tseg-list">'.$child_links.'</ul>';
    }
    else{
        $parent = $post->ID;
        $child_links = wp_list_pages('child_of='.$parent.'&exclude='.$exclude.'&title_li=&echo=0');
        return '<ul class="tseg-list">'.$child_links.'</ul>';
    }
}

add_shortcode('children','tseg_children_shortcode');
?>