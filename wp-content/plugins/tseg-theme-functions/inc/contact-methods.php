<?php
    /**
     *  contact-methods.php
     *
     *  Implements [firm-name] and [phone-number] shortcodes, plus call tracking
     *  logic.
     *
     *  Requires and 'admin' user.
     */

    
    // Sets PPC tracking cookie if ?ppc=true.
    function check_for_ppc_get_attribute()
    {
        // Determine whether this website visitor arrived via PPC ad.
        $ppc = $_GET['ppc'];
        
        if($ppc == 'true')
        {
            // Set the cookie using the current timestamp as the value.
            setcookie(
                'tseg_ppc_campaign', // The name of the cookie.
                time(), // The value of the cookie.
                time() + 157680000, // (5 years) The time the cookie expires.
                COOKIEPATH, // The path on the server in which the cookie will be available on.
                COOKIE_DOMAIN, // The (sub)domain that the cookie is available to.
                false, // When true, cookie should only be transmitted over HTTPS.
                true // When true, cookie will only accessible via HTTP (no javascript access).
            );
        }
    }
    add_action('init', 'check_for_ppc_get_attribute');
    

    // Add some extra settings fields for all users.
    function tseg_contact_methods($contactmethods)
    {
        $contactmethods['contactemail'] = 'Contact E-mail';
        $contactmethods['facebook'] = 'Facebook';
        $contactmethods['fax'] = 'Fax';
        $contactmethods['map'] = 'Google Maps';
        $contactmethods['plus'] = 'Google Plus';
        $contactmethods['linkedin'] = 'LinkedIn';
        $contactmethods['ngage'] = 'Ngage';
        $contactmethods['phone'] = 'Phone (Primary)';
        $contactmethods['phone_ppc'] = 'Phone (PPC Campaign)';
        $contactmethods['twitter'] = 'Twitter';
        $contactmethods['typekit'] = 'Typekit';
        $contactmethods['youtube'] = 'Youtube';
    
        unset($contactmethods['aim']);
        unset($contactmethods['jabber']);
        unset($contactmethods['yim']);
    
        return $contactmethods;
    };
    add_filter('user_contactmethods','tseg_contact_methods',10,1);
    
    
    function tseg_phone_shortcode($raw_attributes = array())
    {
        $attributes = shortcode_atts([
                'numeric' => 'false',
            ],
            $raw_attributes
        );
        
        // Try to get the user 'admin' as an object.
        $admin_object = get_user_by('login', 'admin');
        
        if(!$admin_object)
        {
            // If we can't find 'admin' then return a placeholder.
            return '(555) 555-5555';
        }
        
        // Check for PPC cookie or GET attribute, and if set, use the PPC telephone number.
        if(isset($_COOKIE['tseg_ppc_campaign']) || $_GET['ppc'] == 'true')
        {
            // Make sure this number is set, else use the primary phone number.
            if(!empty($admin_object->phone_ppc))
            {
                $phone_number = $admin_object->phone_ppc;
            }
            else
            {
                $phone_number = $admin_object->phone;
            }
        }
        else
        {
            $phone_number = $admin_object->phone;
        }
        
        // Should we remove everything but the numbers (e.g., for a tel link)?
        if($attributes['numeric'] == 'true')
        {
            $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
        }
        
        return $phone_number;
    };
    add_shortcode('phone-number','tseg_phone_shortcode');
    
    
    function tseg_firm_shortcode($atts = array())
    {
        return get_bloginfo('title');
    }
    add_shortcode('firm-name','tseg_firm_shortcode');

?>