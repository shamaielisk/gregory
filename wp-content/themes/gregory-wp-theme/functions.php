<?php

function enqueue_scripts_and_styles()
{
    if(!is_admin()) {
        
        /**
         *  Deregister WordPress jQuery and replace it with theme-specific jQuery.
         */
        wp_deregister_script('jquery'); 
        wp_register_script('jquery', get_bloginfo('stylesheet_directory') . '/js/jquery-2.2.4.min.js');
        wp_enqueue_script('jquery');
        
        
        /**
         *  Register local bootstrap.min.js.
         */
        wp_register_script('bootstrap-js', get_bloginfo('stylesheet_directory') . '/js/bootstrap.min.js');
        wp_enqueue_script('bootstrap-js');
        
    }
    
    if(!is_admin()) {
        
            
        /**
         *  Register local bootstrap.min.css.
         */
        wp_register_style('bootstrap-css', get_bloginfo('stylesheet_directory') . '/bootstrap.min.css');
		wp_enqueue_style('bootstrap-css');
         wp_enqueue_style('main-styles', get_template_directory_uri().'../style.css');
    }
  
}
add_action('wp_enqueue_scripts', 'enqueue_scripts_and_styles');


// Register sidebar
$sidebar_args = array(
    'id'   => 'sidebar-1',
    'name' => 'Sidebar'
);
register_sidebar($sidebar_args);


add_theme_support('post-thumbnails'); // Enable Featured Images in pages
add_theme_support('menus'); // Enable WordPress menus
wp_create_nav_menu('Header Navigation');
wp_create_nav_menu('Footer Navigation');


function display_error_no_menu()
{
?>
    <div class="alert alert-warning">WordPress is either missing a menu or the menu is empty. Please resolve this issue by logging in to the <a href="/wp-admin/">WordPress Admin Area</a> and selecting "Appearance" then "Menus."</div>
    <?php
}


// Remove Wordpress emoji support (new addition as of Wordpress 4.2)
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/*
 *  Bootstrap Dropdown Menus (without sub-sub-menus)
 *
 *  Don't forget to pass 'walker' => new Bootstrap_Walker_Nav_Menu()
 *  argument in your call to wp_nav_menu().
 */
class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu
{
    function __construct()
    {
        remove_all_filters('nav_menu_css_class');
        remove_all_filters('nav_menu_link_attributes');
        remove_all_filters('nav_menu_item_args');
        add_filter('nav_menu_css_class', [$this, 'add_dropdown_class'], 10, 2);
        add_filter('nav_menu_link_attributes', [$this, 'add_dropdown_toggle_class'], 10, 2);
        add_filter('nav_menu_item_args', [$this, 'add_caret_to_dropdown_link'], 10, 2);
    }
    
    /**
     *  Required: Rewrite <ul> for dropdown submenus.
     *
     *  This walker, along with the functions below, enables Bootstrap's
     *  submenu dropdowns.
     */
    function start_lvl( &$output, $depth = 0, $args = array() )
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\" role=\"menu\">\n";
    }
    
    /**
     *  Required: Add Bootstrap's 'dropdown' class to <li> elements with children.
     */
    function add_dropdown_class($classes, $item = null)
    {
        if ($item != null)
        {
            if (in_array('menu-item-has-children', $classes))
            {
                $classes[] = 'dropdown';
            }
            
            // Add 'tseg-nav__list-item' class to <li>s.
            $classes[] = 'tseg-nav__list-item';
        }
        return $classes;
    }
    
    /**
     *  Optional: Click triggers dropdown. Warning, makes parent link not work.
     *
     *  Add Bootstrap's 'data-toggle="dropdown"' attribute to links belonging to
     *  <li> elements with children.
     */
    function add_dropdown_toggle_class($atts, $item = null, $args = null)
    {
        if (is_array($item->classes))
        {
            if (in_array('menu-item-has-children', $item->classes))
            {
                $atts['data-toggle'] = 'dropdown';
            }
            
            /**
             *  Add 'tseg-nav__link' class to <a>s.
             *
             *  Use 'tseg-nav__link' if link has no class, else append ' tseg-nav__link'.
             */
            array_key_exists('class', $atts) ? $atts['class'] .= ' tseg-nav__link' : $atts['class'] = 'tseg-nav__link';
        }
        return $atts;
    }
    
    /**
     *  Optional: Add Bootstrap's caret character next to dropdown menu links.
     *
     *  Bootstrap 3 ONLY!
     *
     *  This filter modifies the arguments ($args) for the entire menu -- the $args
     *  are shared among all menu items. So once we modify an arg for a dropdown,
     *  we have to make sure and put it back for non-dropdowns.
     */
    function add_caret_to_dropdown_link($args, $item = null, $depth = 0)
    {
        if (is_array($item->classes))
        {
            if (in_array('menu-item-has-children', $item->classes))
            {
                //error_log(print_r($args, true));
                $args->link_after = '<span class="caret"></span>';
            }
            else
            {
                $args->link_after = null;
            }
        }
        return $args;
    }
}


/*
 *  Footer Menus (no dropdowns)
 *
 *  This walker, along with the functions below, will create a menu whose
 *  elements contain custom classes.
 */
class Footer_Walker_Nav_Menu extends Walker_Nav_Menu
{
    function __construct()
    {
        remove_all_filters('nav_menu_css_class');
        remove_all_filters('nav_menu_link_attributes');
        remove_all_filters('nav_menu_item_args');
        add_filter('nav_menu_css_class', [$this, 'add_dropdown_class'], 10, 2);
        add_filter('nav_menu_link_attributes', [$this, 'add_dropdown_toggle_class'], 10, 2);
    }
    
    /**
     *  List Items (<li>s).
     *
     *  Add 'tseg-footer__list-item' class to <li> elements.
     */
    function add_dropdown_class($classes, $item = null)
    {
        if ($item != null)
        {
            $classes[] = 'tseg-footer__list-item';
        }
        return $classes;
    }
    
    /**
     *  Anchors (<a>s).
     *
     *  Add 'tseg-footer__link' class to links belonging to <li> elements.
     */
    function add_dropdown_toggle_class($atts, $item = null, $args = null)
    {
        if ($item != null)
        {
            /**
             *  Use 'tseg-footer__link' if link has no class, else append ' tseg-footer__link'.
             */
            array_key_exists('class', $atts) ? $atts['class'] .= ' tseg-footer__link' : $atts['class'] = 'tseg-footer__link';
        }
        return $atts;
    }
}


// Bootstrap Pagination
if(!function_exists('bootstrap_pagination'))
{
    function bootstrap_pagination()
    {
        // Do pagination, but return an array instead of html.
        global $wp_query;
        $big = 99999; // need an unlikely integer
        $links = paginate_links([
            'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'current'   => max(1, get_query_var('paged')),
            'format'    => '?paged=%#%',
            'next_text' => 'Next',
            'prev_text' => 'Previous',
            'total'     => $wp_query->max_num_pages,
            'type'      => 'array'
        ]);
        
        // If there are no pagination links (i.e., there's only one page), do nothing.
        if(empty($links))
        {
            return;
        }
        
        // Remove default link class and replace it with the Bootstrap class.
        $links = str_replace('page-numbers', 'page-link', $links);
        
        // Create some empty variables.
        $result_html = '';
        $list_items_html = '';
        
        // Wrap each link in a list item.
        foreach($links as $link)
        {
            // CSS classes for all list items (<li>).
            $list_item_classes = [
                'page-item'
            ];
            
            // If link to current page, add 'active' class to this link's <li>.
            if(strpos($link, 'current') !== FALSE)
            {
                $list_item_classes[] = 'active';
            }
            
            // Create a list item with the Bootstrap class(es) for each link.
            $list_items_html .= sprintf('<li class="%2$s">%1$s</li>', $link, join(' ', $list_item_classes));
        }
        
        // CSS classes for the unordered list (<ul>).
        $list_classes = [
            'pagination'
        ];
        
        // Wrap our list items in a <ul>.
        $result_html .= sprintf('<ul class="%2$s">%1$s</ul>', $list_items_html, join(' ', $list_classes));
        
        // Return our Bootstrap 4 pagination list.
        echo $result_html;
    }
}

// lbriray optimized
class TSEG
{
    /**
     *  Returns a <style> section containing the contents of the stylesheet.
     */
    public static function inline_critical_stylesheet( $relative_path )
    {
        $absolute_path = dirname(__FILE__) . $relative_path;
        $file_contents = file_exists( $absolute_path ) ? file_get_contents($absolute_path) : "$absolute_path was not found!";
        return "<style type=\"text/css\">". $file_contents ."</style>";
    }
    
    /**
     *  Returns a <script> section containing the contents of the script file.
     */
    public static function inline_critical_script( $relative_path )
    {
        $absolute_path = dirname(__FILE__) . $relative_path;
        $file_contents = file_exists( $absolute_path ) ? file_get_contents($absolute_path) : "$absolute_path was not found!";
        return "<script type=\"text/javascript\">". $file_contents ."</script>";
    }
}

function set_ppc_header() {
    /* Check For HTTP REFERER and absence of tseg_ppc_referer cookie */
    if(isset($_SERVER['HTTP_REFERER']) && (!isset($_COOKIE['tseg_ppc_referer']))) {
        $referer_host = parse_url($_SERVER['HTTP_REFERER']);
        $ref_host_base = $referer_host['host'];
        if($_SERVER['HTTP_HOST'] !== $ref_host_base) {
        $tseg_ppc = $_SERVER['HTTP_REFERER'];
                echo '<script> document.cookie = "tseg_ppc_referer = '. $tseg_ppc . ';path=/;max-age=2592000"</script>';        }
    }
}
add_action('wp_head','set_ppc_header');


/* Add Referer to Gravity Forms Hidden Field  pls add this to hidden input tseg_ref_url*/
function populate_tseg_ref_url( $value ) {
    if(isset($_COOKIE['tseg_ppc_referer'])):
        return $_COOKIE['tseg_ppc_referer'];
    else:
        return "";
    endif;
}
add_filter( 'gform_field_value_tseg_ref_url', 'populate_tseg_ref_url' );


?>
