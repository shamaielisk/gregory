<?php if(is_home() || is_tag() || is_category() || is_date()): ?>

    <?php while ( have_posts() ): the_post(); ?>
        <div id="post-<?php the_id(); ?>" class="entry">
            <div class="entry__header">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p class="entry__time">Posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> &nbsp; &nbsp; <?php if (is_user_logged_in()) { ?><span class="entry__edit"><?php edit_post_link('Edit Post', '', ''); ?></span><?php } ?></p>
            </div>
            <div class="entry__body">
                <?php the_content(); ?>
            </div>
            <div class="entry__footer">
                <p class="entry__categories"><i class="fa fa-folder-open"></i> Categories: <?php the_category(', '); ?></p>
                <?php $tag = get_the_tags(); if (!$tag) { } else { ?><p class="entry__tags"><i class="fa fa-tags"></i> Tags: <?php the_tags('',', ',''); ?></p><?php } ?>
            </div>
        <?php // only put <hr/> between posts, not after last post ?>
        <?php if ($wp_query->current_post != ($wp_query->post_count - 1)): ?>
            <hr/>
        <?php endif; ?>
        </div>
    <?php endwhile; ?>
    <div class="blog-pagination"><?php bootstrap_pagination(); ?></div>
    
<?php elseif(is_single()): ?>

    <?php while ( have_posts() ): the_post(); ?>
        <div id="post-<?php the_id(); ?>" class="entry">
            <div class="entry__header">
                <h1><?php the_title(); ?></h1>
                <p class="entry__time">Posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> &nbsp; &nbsp; <?php if (is_user_logged_in()) { ?><span class="entry__edit"><?php edit_post_link('Edit Post', '', ''); ?></span><?php } ?></p>
            </div>
            <div class="entry__body">
                <?php the_content(); ?>
            </div>
            <div class="entry__footer">
                <p class="entry__categories"><i class="fa fa-folder-open"></i> Categories: <?php the_category(', '); ?></p>
                <?php $tag = get_the_tags(); if (!$tag) { } else { ?><p class="entry__tags"><i class="fa fa-tags"></i> Tags: <?php the_tags('',', ',''); ?></p><?php } ?>
            </div>
        </div>
    <?php endwhile; ?>
        
<?php elseif(is_page()): ?>

    <?php while ( have_posts() ): the_post(); ?>
        <div class="content">
            <?php the_content(); ?>
        </div>
    <?php endwhile; ?>

<?php else: ?>

    <h1>Page Not Found</h1>
    <p>Sorry, no posts matched your criteria.</p>
    
<?php endif; ?>