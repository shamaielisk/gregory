<div class="sidebar">
    
    <?php if(is_home() || is_single() || is_tag() || is_category() || is_date()): ?>
    
    <div class="panel">
        <div class="panel-heading">
            <h3>Archives</h3>
        </div>
        <div class="panel-body">
            <ul>
                <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 24 ) ); ?>
            </ul>
        </div>
    </div>
    
    <?php else: ?>

        <?php if( !is_page('contact') ): ?>
        
        <div class="panel">
            <div class="panel-heading">
                <h3>Contact Us</h3>
            </div>
            <div class="panel-body">
                <div class="quick-contact">
                    <form>
                        <div class="form-group">
                            <label for="exampleTextInput1">Name</label>
                            <input type="text" class="form-control" id="exampleTextInput1" placeholder="First Last">
                        </div>
                        <div class="form-group">
                            <label for="exampleTextInput2">Phone</label>
                            <input type="text" class="form-control" id="exampleTextInput2" placeholder="(555) 555-5555">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                        </div>
                        <textarea class="form-control" rows="8"></textarea>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        
        <?php endif; ?>

    <?php endif; ?>

</div>