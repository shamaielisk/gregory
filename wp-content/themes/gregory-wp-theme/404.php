<?php 

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage TSEG_Client_Assets
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="tseg-main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                
                <?php if(function_exists('bcn_display')): ?>
                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                    <?php
                        /*
                         *  Display breadcrumbs using the Breadcrumb NavXT plugin, if installed.
                         */
                        bcn_display();
                    ?>
                </div>
                <?php endif; ?>
                
                <?php get_template_part('loop'); ?>
				<section class="error-404 not-found">
                <header class="page-header">
                    <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?></h1>
                </header><!-- .page-header -->
                <div class="page-content">
                    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen' ); ?></p>

                    <?php get_search_form(); ?>

                </div><!-- .page-content -->
            </section><!-- .error-404 -->
                
            </div>
            <div class="col-xs-12 col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>