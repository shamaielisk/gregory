<?php get_header(); ?>

<div class="tseg-main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                
                <?php if(function_exists('bcn_display')): ?>
                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                    <?php
                        /*
                         *  Display breadcrumbs using the Breadcrumb NavXT plugin, if installed.
                         */
                        bcn_display();
                    ?>
                </div>
                <?php endif; ?>
                
                <?php get_template_part('loop'); ?>
                
            </div>
            <div class="col-xs-12 col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>