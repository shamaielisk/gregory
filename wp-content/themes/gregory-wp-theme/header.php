<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        <?php bloginfo('title'); ?>
    </title>
    <?= TSEG::inline_critical_stylesheet('/style.css'); ?>

        <?php wp_head(); ?>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js">
            <!-- Fontawesome 5 files -->    
            <?php /* this is what we should be doing */ ?>
            <!-- Search Psuedo before loading JS files -->

            <
            script > window.FontAwesomeConfig = {
                searchPseudoElements: true
            }

        </script>
        <?= TSEG::inline_critical_script('/font-awesome/js/fontawesome-all.min.js'); ?>
            <?= TSEG::inline_critical_stylesheet('/font-awesome/css/fa-svg-with-js.css'); ?>
                <!-- Fontawesome 5 files -->

</head>

<body <?php body_class(); ?>>

    <!--Header & navigation  -->
    <header class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <?php
//			wp_nav_menu( array(
//				'theme_location' => 'Header Menu',
//				'menu_id'        => 'header_menu',
//			) );
                        
             wp_nav_menu( array(
				'menu' => 'header_menu',
				
			) );
			?>

                    </div>
                </div>
            </nav>
        </div>
    </header>
