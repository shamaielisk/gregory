<?php get_header(); ?>

<div class="tseg-main-graphic">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-offset-md-2">
                <div class="tseg-main-graphic__tagline">
                    This is a main graphic tagline!
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tseg-home-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php get_template_part('loop'); ?>
            </div>
        </div>
    </div>
</div>

<div class="tseg-grid-of-links">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item One</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Two</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Three</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Four</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Five</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Six</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Seven</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Eight</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Nine</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Ten</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Eleven</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="#" class="tseg-grid-of-links__link btn btn-default btn-lg"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i> Link Item Twelve</a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>