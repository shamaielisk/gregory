<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gregory');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N[%dkfk`EuHI`*1w7QN{ie(~&~42L> >C>gt#Zu(mx*ATkr+9`H>aiLGihHCn]Bm');
define('SECURE_AUTH_KEY',  ';F.|T<^1|1BnFLU{v:p&m@^6%}Y}ydr/llAmIz2m8jm007`@Q0@*][+1pTM[x6Ev');
define('LOGGED_IN_KEY',    'iLrnR,ZRVhq`ao8z`2WNh}2>JW)9|Y91_LU,bKW+$AR0WQU<7?hQQyd02$CgE`/K');
define('NONCE_KEY',        'hpUKipfM+Tsd-ok7?!;WwYJ+pp8c^}dqDVqcP?:WEtIA34zs-ub8xaqV<]< Fi<=');
define('AUTH_SALT',        'Rhk%f!L&[zLgY4.eDpV`#aPvs(K+GWT1-B?=4%ay|>8{:msaCJZ?sZ:mILd0?VO&');
define('SECURE_AUTH_SALT', '[1/u},I@<j3RGLIstqX P`Oqt6v~*^*)w%%YcKea&q6.nt5+)|OtD`!0HyHopFVY');
define('LOGGED_IN_SALT',   'vC4;+NAFYt#yI>ACSYb7tUJ8hPt(^Yu;m1f8Qhg@2(=S06L$K?3[I@ndp[M,H.?W');
define('NONCE_SALT',       'mp+FL+niAip!^nz;n9BcB%b.?{FGekZS))ZP}P@i)fHS>q9L-.Avo2A..Ap36FLu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
